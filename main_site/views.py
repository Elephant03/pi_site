'''
Renders the webpages when these functions are called
'''
from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login as user_login
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm

from .forms import ContactForm

from.my_email import send_email


# Create your views here.


def front_page(request):
    '''
    Renders the websites front page
    '''
    return render(request, 'main_site/front_page.html', {})


@ login_required
@ permission_required('main_site.simba_access')
def simba(request):
    '''
    Renders the page full of simba's stuff
    '''
    return render(request, 'main_site/simba.html', {})


def about_us(request):
    '''
    Renders the about page
    '''
    return render(request, 'main_site/about_us.html', {})


def login(request):
    '''
    loads the login page
    '''
    return render(request, 'main_site/login.html', {})


def signup(request):
    '''
    Loads the signup page
    '''
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(username=form.cleaned_data.get('username'),
                                password=form.cleaned_data.get('password1'))
            user_login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()

    return render(request, 'registration/sign_up.html', {'form': form})


def contact_us(request):
    '''
    Renders the contact form
    '''
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            sender_name = form.cleaned_data['name']
            sender_email = form.cleaned_data['email']

            message = "{0}({1}) has sent you a new message:\n\n{2}".format(
                sender_name, sender_email, form.cleaned_data['message'])

            send_email(message)

            return redirect('thank_you')
    else:
        form = ContactForm()

    return render(request, 'main_site/contact_us.html', {'form': form})


def thank_you(request):
    '''
    renders thank you page
    '''
    return render(request, 'main_site/thank_you.html', {})
