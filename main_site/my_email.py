'''
Sends emails from the contact us form
'''
import smtplib
import json


def send_email(message):
    '''
    Sends an email message
    '''
    with open("{}/static/emaildata.json".format(__file__[:-22].replace("\\", "/")), "r") as email_json:
        json_data = json.load(email_json)
        gmail_user = json_data["email_adress"]
        gmail_password = json_data["email_password"]
        to_adress = json_data["to_adress"]

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(gmail_user, to_adress, message)
        server.close()

        return True
    except Exception as identifier:
        print(identifier)
        return False