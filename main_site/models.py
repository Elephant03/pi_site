'''
Stores the models for the website
'''
from django.db import models
#from django.conf import settings

# Create your models here.


class Perms(models.Model):
    '''
    # a model to set permissions for users
    '''

    # name = models.ForeignKey(settings.AUTH_USER_MODEL,
    #                         on_delete=models.CASCADE)
    class Meta:
        '''
        Stores the custom permissions
        '''
        permissions = (("simba_access", "Access the simba url"),)
