'''
Stores the url links to my webpages
'''
from django.urls import path
from . import views

urlpatterns = [
    path('', views.front_page, name='front_page'),
    path('simba', views.simba, name="simba"),
    path('join_us', views.signup, name="join_us"),
    path('about', views.about_us, name="about_us"),
    path('contact_us', views.contact_us, name="contact_us"),
    path('thank_you', views.thank_you, name='thank_you')
]
