function swap_grass() {
    var mySrc = grassImage.getAttribute('src');
    var newPhotoID = grassPhotos.indexOf(mySrc) + 1

    if (newPhotoID === lastPhotoID) {
        newPhotoID = 0
    };

    grassImage.setAttribute('src', grassPhotos[newPhotoID]);
}

var grassImage = document.getElementById("grass");
var grassBtn = document.querySelector("button");

var grassPhotos = [
    "/static/main_site/images/grass/grass1.jpg",
    "/static/main_site/images/grass/grass2.jpg",
    "/static/main_site/images/grass/grass3.jpg",
    "/static/main_site/images/grass/grass4.jpg",
]

var lastPhotoID = grassPhotos.length;

grassBtn.onclick = function () {
    swap_grass();
}