var sideBarExtended = false;

// This means the menu button can close the sidebar
function toggle() {
    if (sideBarExtended) {
        closeNav();
    } else {
        openNav();
    };

    sideBarExtended = !sideBarExtended;
}

function openNav() {
    var sideNav = document.getElementById("mySidenav");
    sideNav.style.width = "200px";
    sideNav.style.paddingLeft = "30px";
    document.getElementById("main").style.marginRight = "200px";
}

function closeNav() {
    var sideNav = document.getElementById("mySidenav");
    sideNav.style.width = "0";
    sideNav.style.paddingLeft = "0";
    document.getElementById("main").style.marginRight = "0";
}